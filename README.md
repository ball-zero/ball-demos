# Demos and Tutorial results

These complete configured networks after following the turtorial (more or less).

Each subdirectory will hold one network.  The subdirectories of that
network each hold the repository of a representative.

A script `run.sh` in the network's directory should start the demo.
It assumes to find the checkout of the [ball source
repository](https://gitlab.com/ball-zero/ball) as a sibling of this
directory under the name `ball`.

Configured to connect to peers over localhost.  Including all the
required certificates and private keys.  The password for the CA is
"2017".  Having disclosed the keys here you should be warned to NOT
trust them for any purpose than the running the demos.

