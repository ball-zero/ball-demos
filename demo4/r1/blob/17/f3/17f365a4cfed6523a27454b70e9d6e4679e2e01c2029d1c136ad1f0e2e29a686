<?xml version="1.0" encoding="UTF-8" ?>
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Testing publicly readable documents.</title>
  <meta content="Order" name="ContractType"></meta>
  <meta content="Afc9ee611c2bd47c62ac71759c05c709f" name="From"></meta>
  <meta content="A160b485bfe8adab4848183e8f9cbbd79" name="To"></meta>
  <meta content="1" name="Amount"></meta>
  <meta content="A35c6e934f4f98fe37391ccb7c5347ee7" name="Instrument"></meta>
  <meta content="application/xhtml+xml; charset=UTF-8" http-equiv="Content-Type"></meta>
  <link href="/A30b62e000c07ae6fe2918739db5a4400" rel="icon" type="image/x-icon"></link>
  <style type="text/css">.amount { background: aquamarine; padding: 0.2ex; text-align: right; }
.asset { background: goldenrod; padding: 0.2ex; }
.liability { color: red; }
.sender { background: lightcoral; }
.receiver { background: darkseagreen; }
.subject { background: cornsilk; }
.terms { color: cornflowerblue;}
.intern { color: red; }
.extern { color: green; }
.order { background: orange; }
.receipt { background: antiquewhite; }
.notary { background: lightcyan; }
.attachment { background: beige; }
.help { background: lavender; }
.fineprint { background: beige; font-size: smaller; }
.executable { background: gainsboro; font-family: monospace; white-space:pre; }
method { background: gainsboro; border: 1px dashed; margin: 1em; padding: 1em; }
td.amount, td.empty { width: 5ex;}
.menu { text-decoration: none; }</style>
 </head>
 <body>
  <div class="header"></div>
  <div class="preamble"></div>
  <div class="content">
   <h1 class="order formal">Order</h1>
   <p class="formal"><label>I, </label><em class="sender" title="account title at time of signature">Alice</em><label>, the signatory, send </label><strong class="amount" title="amount">1</strong>
in <strong class="asset"><a href="A35c6e934f4f98fe37391ccb7c5347ee7" title="A35c6e934f4f98fe37391ccb7c5347ee7">Euro</a> OID: <code>A35c6e934f4f98fe37391ccb7c5347ee7</code></strong><label> at </label><span class="date">2018-01-22 20:03:30</span><label> for</label></p>
   <blockquote class="subject">
    <p title="Title">Testing publicly readable documents.</p>
   </blockquote>
   <p class="formal">
    <label>from account </label>
    <code>
     <a class="sender" href="/Afc9ee611c2bd47c62ac71759c05c709f" title="outgoing account">Afc9ee611c2bd47c62ac71759c05c709f</a>
    </code>
    <label> to account </label>
    <a class="receiver" href="/A160b485bfe8adab4848183e8f9cbbd79" title="receiving account">Bob OID: <code>A160b485bfe8adab4848183e8f9cbbd79</code></a>
    <label> .</label>
   </p>
   <div class="terms">
    <div>
     <p>This order should be publicly readable.</p>
     <p>(Note that I ost a few steps because the "metaview" does <em>not</em> work with wallets to change the protection value.)</p>
    </div>
   </div>
  </div>
  <hr title="generic fine-print below"></hr>
  <blockquote class="fineprint" title="generic fine-print">
   <h2>Audit Record</h2>
   <p class="terms">This is an immutable document maintained according these <a href="/A816d7be99457dc492daa7ea5dcd3d29e" title="service contract, a.k.a master contract or meta contract">terms and conditions</a>.</p>
   <p>At Mon, 22 Jan 2018 20:03:30 +0100
the notaries below confirmed
<a class="sender" href="/A1a5d068f9c4abf9cdaa627f7183b5d5c">my signature</a>.</p>
   <ol>
    <li>
     <a class="notary" href="/Ae95c0af22ef281c9f7242d01bca0d51f" title="Ae95c0af22ef281c9f7242d01bca0d51f">Ae95c0af22ef281c9f7242d01bca0d51f</a>
    </li>
    <li>
     <a class="notary" href="/A74805e3acdcdc4a621cfb21df1291046" title="A74805e3acdcdc4a621cfb21df1291046">A74805e3acdcdc4a621cfb21df1291046</a>
    </li>
    <li>
     <a class="notary" href="/A85c9b1fb78966588e9951cbe124b806c" title="A85c9b1fb78966588e9951cbe124b806c">A85c9b1fb78966588e9951cbe124b806c</a>
    </li>
    <li>
     <a class="notary" href="/A0c66f2f41b501f1378563624bcbb818b" title="A0c66f2f41b501f1378563624bcbb818b">A0c66f2f41b501f1378563624bcbb818b</a>
    </li>
   </ol>
   <p class="help">To aquire a proof from these notaries make sure to
  use different representative (peer devices), or locate the IP address
  of those and connect them directly.</p>
   <h3>Notational conventions</h3>
   <p>The following CSS classes call out specific text elements.
Often there is additional information in the title of text elements (mouse over).</p>
   <p>At the moment these styles interfere heavily with useability.
I welcome especially: suggestions on best common law phrase to use for the styles,
preferrable colors and concise wording for the description.</p>
   <dl compact="compact" title="CSS classes">
    <dt class="amount" style="text-align:left;">amount</dt>
    <dd>units transferred</dd>
    <dt class="asset">asset</dt>
    <dd>assets, payment instruments</dd>
    <dt class="sender">sender</dt>
    <dd>outgoing account and signatory</dd>
    <dt class="receiver">receiver</dt>
    <dd>account receiving</dd>
    <dt class="subject">subject</dt>
    <dd>title or subject</dd>
    <dt class="terms">terms</dt>
    <dd>terms&amp;conditions or optional message text; free form</dd>
    <dt class="extern">extern</dt>
    <dd>link to be transferred to the receiver</dd>
    <dt class="intern">intern</dt>
    <dd>link possibly containing login information
(if anon access code is used) <strong>not safe to transfer</strong></dd>
    <dt class="order">order</dt>
    <dd>link to an order</dd>
    <dt class="receipt">receipt</dt>
    <dd>link to a receipt</dd>
    <dt class="notary">notary</dt>
    <dd>link to notary commissioned to audit this document</dd>
    <dt class="attachment">attachment</dt>
    <dd>link to attachment</dd>
    <dt class="fineprint">fineprint</dt>
    <dd>generic fine-print</dd>
    <dt class="executable">executable</dt>
    <dd>machine executable source code</dd>
    <dt class="help">help</dt>
    <dd>meta talk – explanatory text</dd>
   </dl>
   <p>See also the <a href="/A0cd6168e9408c9c095f700d7c6ec3224?_v=wiki&amp;_id=1728">online documentation</a>.</p>
   <div class="help">
    <p>All objects are identified by their <a href="http://askemos.org/index.html?_v=footnote&amp;_id=866">OID</a>,
   which is a cryptographic hash of a) the OID of the creator of the
   object b) date of creation c) the OID of the contract defining the
   type and d) the hash of the (initial) content of the object.</p>
    <p>Experts see <a href="?xmlns=a" title="low level access">meta data</a> for detailed audit.
(Note that this exposes control forms.  Those are void, the corresponding control API is disabled.)</p>
    <p>Wallet (basic documentary edition) version: 0.4</p>
   </div>
  </blockquote>
  <div class="footer"></div>
 </body>
</html>
